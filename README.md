# OpenML dataset: BreastCancer_Anomaly

https://www.openml.org/d/40894

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

&quot;The features of the breast-cancer dataset are extracted from medical images of a fine needle aspirate (FNA) describing the cell nuclei. The task of the UCI dataset is to separate cancer from healthy patients. From the 212 malignant instances, we kept the first 10 as anomalies. This results in a unsupervised anomaly detection dataset containing 367 instances in total and having 2.72% anomalies.&quot; (cite from Goldstein, Markus, and Seiichi Uchida. &quot;A comparative evaluation of unsupervised anomaly detection algorithms for multivariate data.&quot; PloS one 11.4 (2016): e0152173). This dataset is not the original dataset. The target variable &quot;Target&quot; is relabeled into &quot;Normal&quot; and &quot;Anomaly&quot;.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40894) of an [OpenML dataset](https://www.openml.org/d/40894). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40894/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40894/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40894/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

